package demo;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Before("execution(* demo.ShoppingCart.checkout(..))")
    public void beforeLogger(JoinPoint joinPoint){
        final Signature signature = joinPoint.getSignature();
        //System.out.println(signature);
        final String argument = joinPoint.getArgs()[0].toString();
        System.out.println("Before Loggers with argument: "+argument);
    }

    @After("execution(* *.*.checkout(..)))")
    public void afterLogger(){
        System.out.println("After Loggers");
    }

    @Pointcut("execution(* demo.ShoppingCart.quantity(..))")
    public void afterReturningPoinCut(){

    }

    @AfterReturning(pointcut = "afterReturningPoinCut()",
    returning = "value")
    public void afterReturning(String value){
        System.out.println("After Returning: "+ value);
    }
}
